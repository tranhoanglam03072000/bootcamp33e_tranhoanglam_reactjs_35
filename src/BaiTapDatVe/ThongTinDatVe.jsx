import React, { Component } from "react";
import { connect } from "react-redux";
import {
  huyGheAction,
  xacNhanDatAction,
} from "./Redux/action/BaiTapDatVeAction";

class ThongTinDatVe extends Component {
  renderThongTinDatGhe = () => {
    return this.props.danhSachGheDangDat.map((ghe, index) => {
      return (
        <tr key={index}>
          <td className="py-4 pr-8">{ghe.soGhe}</td>
          <td>{ghe.gia.toLocaleString()}$</td>
          <td className="text-center">
            <button
              onClick={() => {
                this.props.huyGhe(ghe.soGhe);
              }}
              className="text-red-600"
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h1 className="text-white text-3xl text-center mb-14">
          Sách Ghế Bạn Danh Chọn
        </h1>
        <button className="gheDuocChon"></button>:{" "}
        <span className="text-white text-2xl">Ghế đã đặt</span>
        <br />
        <button className="gheDangChon"></button>:{" "}
        <span className="text-white text-2xl">Ghế đang chọn</span>
        <br />
        <button className="ghe" style={{ marginLeft: "0" }}></button>:{" "}
        <span className="text-white text-2xl">Ghế chưa đặt</span> <br />
        {/* // render thông tin  */}
        <div className="mt-8 mb-8">
          <div className="overflow-x-auto relative">
            <table className="w-full text-sm  text-gray-500 dark:text-gray-400">
              <thead className="text-2xl text-left text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th>Số ghế</th>
                  <th>giá</th>
                  <th>huỷ</th>
                </tr>
              </thead>
              <tbody className="text-xl bg-white border-b">
                {this.renderThongTinDatGhe()}
              </tbody>
              <tfoot>
                <tr className="bg-white border-b  text-2xl">
                  <td>Tổng tiền</td>
                  <td className="text-center" colSpan={2}>
                    {this.props.danhSachGheDangDat
                      .reduce((tongTien, ghe, index) => {
                        return (tongTien += ghe.gia);
                      }, 0)
                      .toLocaleString()}
                    $
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <button
          onClick={() => this.props.xacNhanDat()}
          type="button"
          className="focus:outline-none text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:ring-purple-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-900"
        >
          Xác nhận đặt
        </button>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BaiTapDatVeReducer.danhSachGheDangDat,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    huyGhe: (soGhe) => {
      dispatch(huyGheAction(soGhe));
    },
    xacNhanDat: () => {
      dispatch(xacNhanDatAction());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ThongTinDatVe);
