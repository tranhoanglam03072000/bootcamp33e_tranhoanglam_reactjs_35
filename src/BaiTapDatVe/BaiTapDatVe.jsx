import React, { Component } from "react";
import { connect } from "react-redux";
import "./BaiTapDatVeCss.css";
import { dataGhe } from "./data/dataGhe";
import HangGhe from "./HangGhe";
import ThongTinDatVe from "./ThongTinDatVe";

class BaiTapDatVe extends Component {
  renderTatCaGhe = () => {
    return this.props.dataGhe.map((hangGhe, index) => {
      return <HangGhe viTri={index} key={index} hangGhe={hangGhe} />;
    });
  };
  render() {
    return (
      <div className="bookingMovie">
        <div
          className="backGround"
          style={{
            background: "url(./img/bgmovie.jpg)",
            width: "100vw",
            height: "100vh",
            backgroundSize: "cover",
            position: "fixed",
            zIndex: "-1",
          }}
        ></div>
        <div className="container mx-auto">
          <div className="grid grid-cols-3 gap-4">
            <div className="col-span-2 flex flex-col items-center">
              <h1 className="text-orange-400 text-5xl mb-6">Đặt vé xem phim</h1>
              <h2 className="text-white">màn hình</h2>
              <div className="screen"></div>
              <div className="w-full mt-20">{this.renderTatCaGhe()}</div>
            </div>
            {/* //Thông tin đặt vé  */}
            <div>
              <ThongTinDatVe />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    dataGhe: state.BaiTapDatVeReducer.dataGhe,
  };
};
export default connect(mapStateToProps)(BaiTapDatVe);
