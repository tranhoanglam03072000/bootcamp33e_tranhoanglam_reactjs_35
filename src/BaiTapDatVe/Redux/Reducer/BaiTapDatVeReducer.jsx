import { dataGhe } from "../../data/dataGhe";
import {
  DAT_GHE,
  HUY_GHE,
  XAC_NHAN_DAT,
} from "../constant/BaiTapDatVeConstant";

const intialial = {
  danhSachGheDangDat: [],
  dataGhe: dataGhe,
};
export let BaiTapDatVeReducer = (state = intialial, action) => {
  switch (action.type) {
    case DAT_GHE:
      {
        let newDanhSachGheDangDat = [...state.danhSachGheDangDat];
        let index = newDanhSachGheDangDat.findIndex(
          (ghe) => ghe.soGhe == action.payload.soGhe
        );
        if (index == -1) {
          newDanhSachGheDangDat.push(action.payload);
        } else {
          newDanhSachGheDangDat.splice(index, 1);
        }
        state.danhSachGheDangDat = newDanhSachGheDangDat;

        return { ...state };
      }
      break;
    case HUY_GHE:
      {
        let newDanhSachGheDangDat = [...state.danhSachGheDangDat];
        let index = newDanhSachGheDangDat.findIndex(
          (ghe) => ghe.soGhe == action.payload
        );
        if (index != -1) {
          newDanhSachGheDangDat.splice(index, 1);
        }
        state.danhSachGheDangDat = newDanhSachGheDangDat;
        return { ...state };
      }
      break;
    case XAC_NHAN_DAT: {
      let newDataGhe = [...state.dataGhe];
      let newDanhSachGheDangDat = [...state.danhSachGheDangDat];
      newDataGhe.forEach((thongTinHangGhe) => {
        thongTinHangGhe.danhSachGhe.forEach((ghe) => {
          state.danhSachGheDangDat.forEach((gheDangDat) => {
            if (gheDangDat.soGhe == ghe.soGhe) {
              ghe.daDat = true;
            }
          });
        });
      });
      state.dataGhe = newDataGhe;
      newDanhSachGheDangDat = [];
      state.danhSachGheDangDat = newDanhSachGheDangDat;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
