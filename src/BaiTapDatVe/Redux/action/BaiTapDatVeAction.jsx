import {
  DAT_GHE,
  HUY_GHE,
  XAC_NHAN_DAT,
} from "../constant/BaiTapDatVeConstant";

export const datGheAction = (ghe) => {
  return {
    type: DAT_GHE,
    payload: ghe,
  };
};
export const huyGheAction = (soGhe) => {
  return {
    type: HUY_GHE,
    payload: soGhe,
  };
};
export const xacNhanDatAction = () => {
  return {
    type: XAC_NHAN_DAT,
  };
};
