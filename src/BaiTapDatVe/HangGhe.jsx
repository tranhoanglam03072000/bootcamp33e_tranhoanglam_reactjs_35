import React, { Component } from "react";
import { connect } from "react-redux";
import { datGheAction } from "./Redux/action/BaiTapDatVeAction";

class HangGhe extends Component {
  //-----------------------------------------------render hàng đầu tiên

  renderHangGheDauTien = (ghe, index) => {
    return (
      <span
        key={index}
        style={{
          marginLeft: "2%",
          width: "45px",
          height: "35px",
          display: "inline-block",
        }}
        className="text-yellow-200 text-2xl text-center"
      >
        {ghe.soGhe}
      </span>
    );
  };
  //-------------------------------------------Render những hàng sau
  renderHangGhePhiaSau = (ghe, index) => {
    let cssGheDaDat = "";
    let disabled = false;
    let gheDangChon = "";
    // Những ghế đã đặt rồi
    if (ghe.daDat) {
      cssGheDaDat = "gheDuocChon";
      disabled = true;
    }
    //Những ghế đang đặt
    let indexGheDangDat = this.props.danhSachGheDangDat.findIndex(
      (item) => item.soGhe == ghe.soGhe
    );
    if (indexGheDangDat != -1) {
      gheDangChon = "gheDangChon";
    }
    return (
      <button
        onClick={() => {
          this.props.datGhe(ghe);
        }}
        disabled={disabled}
        key={index}
        className={`ghe ${cssGheDaDat} ${gheDangChon}`}
      >
        {ghe.soGhe}
      </button>
    );
  };
  //------------------------------------------render tất cả hàng ghế
  renderHangGhe = () => {
    return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
      //render hàng đầu tiên
      if (this.props.viTri == 0) {
        return this.renderHangGheDauTien(ghe, index);
      }
      //render Hàng ghế còn lại
      return this.renderHangGhePhiaSau(ghe, index);
    });
  };
  render() {
    return (
      <div className="w-full grid grid-cols-12">
        <div className="text-right text-yellow-200 text-2xl">
          {this.props.hangGhe.hang}
        </div>
        <div className="col-span-11">{this.renderHangGhe()}</div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.BaiTapDatVeReducer.danhSachGheDangDat,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    datGhe: (ghe) => {
      dispatch(datGheAction(ghe));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HangGhe);
